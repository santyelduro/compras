﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compras.Repository
{
    public interface IClienteRepository
    {
        Task<List<Cliente>> ListarClientes();
        Task<bool> CrearCliente(Cliente cliente);
        Task<Cliente> ConsultarCliente(int? id);
        Task<bool> EncontrarCliente(int? id);
        Task<bool> ActualizarCliente(Cliente cliente);

        Task<bool> EliminarCliente(int? id);
    }
}
