﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compras.Repository
{
    public interface IOrdenRepository
    {
        Task<List<Ordene>> ListarOrdens();
        Task<bool> CrearOrden(Ordene ordene);
        Task<Ordene> ConsultarOrden(int? id);
        Task<bool> ActualizarOrden(Ordene ordene);
        Task<bool> EliminarOrden(int? id);
        Task<bool> EncontrarOrden(int? id);
    }
}
