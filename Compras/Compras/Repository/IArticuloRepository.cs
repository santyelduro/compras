﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compras.Repository
{
    public interface IArticuloRepository
    {
        Task<List<Articulo>> ListarArticulos();
        Task<bool> CrearArticulo(Articulo Articulo);
        Task<Articulo> ConsultarArticulo(int? id);
        Task<bool> EncontrarArticulo(int? id);
        Task<bool> ActualizarArticulo(Articulo articulo);
        Task<bool> EliminarArticulo(int? id);
    }
}
