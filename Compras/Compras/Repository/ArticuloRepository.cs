﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compras.Repository
{
    public class ArticuloRepository: IArticuloRepository
    {
        private readonly ComprasContext _ComprasContext;
        public ArticuloRepository(ComprasContext iComprasContext)
        {
            _ComprasContext = iComprasContext;
        }

        public async Task<List<Articulo>> ListarArticulos()
        {
            var Articulos = _ComprasContext.Articulos.ToList();

            return Articulos;
        }

        public async Task<bool> CrearArticulo(Articulo articulo)
        {
           
            try
            {
                 _ComprasContext.Articulos.AddAsync(articulo);
                await _ComprasContext.SaveChangesAsync();
            }
            catch (Exception e)
            {

                throw e;
            }
            bool valor = false;
            //if (RespuestaArticulo == null)
            //{
            //    valor = true;
            //}
            return valor;
        }

        public async Task<Articulo> ConsultarArticulo(int? id)
        {
            var Articulo = await _ComprasContext.Articulos.FindAsync(id);
            return Articulo;
        }

        public async Task<bool> ActualizarArticulo(Articulo articulo)
        {
            _ComprasContext.Update(articulo);
            var result = await _ComprasContext.SaveChangesAsync();
            bool valor = true;
            //if (result == null)
            //{
            //    valor = true;
            //}
            return valor;
        }


        public async Task<bool> EliminarArticulo(int? id)
        {
            var articulo = await _ComprasContext.Articulos.FindAsync(id);
            _ComprasContext.Articulos.Remove(articulo);
            var result = await _ComprasContext.SaveChangesAsync();
            bool valor = false;
            if (result == null)
            {
                valor = true;
            }
            return valor;
        }

        public async Task<bool> EncontrarArticulo(int? id)
        {
            return _ComprasContext.Articulos.Any(e => e.IdArticulo == id);
        }
    }
}
