﻿using Compras.dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compras.Repository
{
    public class OrdenRepository: IOrdenRepository
    {
        private readonly ComprasContext _ComprasContext;
        public OrdenRepository(ComprasContext iComprasContext)
        {
            _ComprasContext = iComprasContext;
        }

        public async Task<List<Ordene>> ListarOrdens()
        {
            
           // List<dtoOrden> dtoOrdenes = new List<dtoOrden>();
            var result  = _ComprasContext.Ordenes.ToList();

            return result;
        }

        public async Task<bool> CrearOrden(Ordene ordene)
        {
           await _ComprasContext.Ordenes.AddAsync(ordene);
            
            var RespuestaOrden = await _ComprasContext.SaveChangesAsync();
            bool valor = true;
            //if (RespuestaOrden == null)
            //{
            //    valor = true;
            //}
            return valor;
        }

        public async Task<Ordene> ConsultarOrden(int? id)
        {
            var ordene = await _ComprasContext.Ordenes
                 .Include(o => o.IdArticuloNavigation)
                 .Include(o => o.IdClienteNavigation)
                 .FirstOrDefaultAsync(m => m.IdCliente == id);
            return ordene;
        }

        public async Task<bool> ActualizarOrden(Ordene ordene)
        {
            try
            {
                _ComprasContext.Update(ordene);
                bool result = await _ComprasContext.SaveChangesAsync() >= 0;
            }
            catch (Exception e)
            {

                throw e;
            }
          
            bool valor = true;
            //if (result == null)
            //{
            //    valor = true;
            //}
            return valor;
        }


        public async Task<bool> EliminarOrden(int? id)
        {
            var ordene = await _ComprasContext.Ordenes.FindAsync(id);
            //ViewBag.UserSkills.Find(WebSecurity.CurrentUserId, item.SkillId);
            _ComprasContext.Ordenes.Remove(ordene);
            var result = await _ComprasContext.SaveChangesAsync();
            bool valor = false;
            if (result == null)
            {
                valor = true;
            }
            return valor;
        }

        public async Task<bool> EncontrarOrden(int? id)
        {
            return _ComprasContext.Ordenes.Any(e => e.IdCliente == id);
        }

    }
}
