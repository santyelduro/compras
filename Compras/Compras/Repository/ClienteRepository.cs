﻿using Compras.dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compras.Repository
{
    public class ClienteRepository: IClienteRepository
    {
        private readonly ComprasContext _ComprasContext;

        public ClienteRepository(ComprasContext iComprasContext)
        {
            _ComprasContext = iComprasContext;
        }

        public async Task<List<Cliente>> ListarClientes()
        {
            var Clientes = _ComprasContext.Clientes.ToList();
            
            return Clientes;
        }

        public async Task<bool> CrearCliente(Cliente cliente)
        {
            await _ComprasContext.Clientes.AddAsync(cliente);
            await _ComprasContext.SaveChangesAsync();
            bool valor = true;
            //if (RespuestaCliente==null)
            //{
            //    valor = true;
            //}
            return valor;
        }

        public async Task<Cliente> ConsultarCliente(int? id)
        {
            var cliente = await _ComprasContext.Clientes.FindAsync(id);
            return cliente;
        }

        public async Task<bool> ActualizarCliente(Cliente cliente)
        {
            _ComprasContext.Update(cliente);
            var result = await _ComprasContext.SaveChangesAsync();
            bool valor = true;
            //if (result == null)
            //{
            //    valor = true;
            //}
            return valor;
        }


        public async Task<bool> EliminarCliente(int? id)
        {
            var persona = await _ComprasContext.Clientes.FindAsync(id);
            _ComprasContext.Clientes.Remove(persona);
            var result = await _ComprasContext.SaveChangesAsync();
            bool valor = true;
            //if (result == null)
            //{
            //    valor = true;
            //}
            return valor;
        }

        public async Task<bool> EncontrarCliente(int? id)
        {
            return _ComprasContext.Clientes.Any(e => e.IdCliente == id);
        }


    }
}
