﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Compras;
using Compras.Services;
using Compras.dto;

namespace Compras.Controllers
{
    public class OrdenesController : Controller
    {
        private readonly ComprasContext _context;
        private readonly IOrdenService _iOrdenService;
        private readonly IClienteService _iClienteService;
        private readonly IArticuloService _iArticuloService;

        public OrdenesController(
            IArticuloService iArticuloService,
            IClienteService iClienteService,
            IOrdenService iOrdenService,
            ComprasContext context)
        {
            _context = context;
            _iOrdenService = iOrdenService;
            _iClienteService = iClienteService;
            _iArticuloService = iArticuloService;
        }

        // GET: Ordenes
        public async Task<IActionResult> Index()
        {
            
            List<dtoOrden> dtoOrdenes = await _iOrdenService.ListarOrdenes();
            return View(dtoOrdenes);
        }

        // GET: Ordenes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //var ordene = await _context.Ordenes
            //    .Include(o => o.IdArticuloNavigation)
            //    .Include(o => o.IdClienteNavigation)
            //    .FirstOrDefaultAsync(m => m.IdCliente == id);
            dtoOrden ordene = await _iOrdenService.ConsultarOrden(id);
            if (ordene == null)
            {
                return NotFound();
            }
            dtoCliente dtoCliente = new dtoCliente();
            dtoCliente = await _iClienteService.ConsultarCliente(id);

            dtoArticulo dtoArticulo = new dtoArticulo();
            dtoArticulo = await _iArticuloService.ConsultarArticulo(ordene.IdArticulo);

            ordene.NombreCliente = dtoCliente.Nombre;
            ordene.NombreArticulo = dtoArticulo.NombreArticulo;
            return View(ordene);
        }

        // GET: Ordenes/Create
        public async Task<IActionResult> Create()
        {
            dtoOrden dtoOrden = new dtoOrden();
            dtoOrden.Clientes = await _iClienteService.ListarClientes();
            dtoOrden.Articulos = await _iArticuloService.ListarArticulos();
            return View(dtoOrden);
        }

        // POST: Ordenes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdCliente,IdArticulo,FechaOrden,Cantidad")] dtoOrden ordene)
        {
            if (ModelState.IsValid)
            {
                var respuesta = await _iOrdenService.CrearOrden(ordene);
                return RedirectToAction(nameof(Index));
            }
            
            return View(ordene);
        }

        // GET: Ordenes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            dtoOrden ordene = await _iOrdenService.ConsultarOrden(id);
            if (ordene == null)
            {
                return NotFound();
            }
            // dtoOrden dtoOrden = new dtoOrden();
            ordene.Clientes = await _iClienteService.ListarClientes();
            ordene.Articulos = await _iArticuloService.ListarArticulos();
            return View(ordene);
        }

        // POST: Ordenes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdOrden,IdCliente,IdArticulo,FechaOrden,Cantidad")] dtoOrden ordene)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    var result = await _iOrdenService.ActualizarOrden(ordene);
                    
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await OrdeneExists(ordene.IdCliente))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ordene.Clientes = await _iClienteService.ListarClientes();
            ordene.Articulos = await _iArticuloService.ListarArticulos();
            //ViewData["IdArticulo"] = new SelectList(_context.Articulos, "IdArticulo", "IdArticulo", ordene.IdArticulo);
            //ViewData["IdCliente"] = new SelectList(_context.Clientes, "IdCliente", "IdCliente", ordene.IdCliente);

            return View(ordene);
        }

        // GET: Ordenes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            dtoOrden ordene = await _iOrdenService.ConsultarOrden(id);
            if (ordene == null)
            {
                return NotFound();
            }

            return View(ordene);
        }

        // POST: Ordenes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            //var ordene = await _context.Ordenes.FindAsync(id);
            //_context.Ordenes.Remove(ordene);
            //await _context.SaveChangesAsync();
            dtoOrden dtoOrden = await _iOrdenService.ConsultarOrden(id);
            bool result = await _iOrdenService.EliminarOrden(dtoOrden.IdCliente);
            return RedirectToAction(nameof(Index));
        }

        public async Task<bool> OrdeneExists(int id)
        {
            return await _iOrdenService.EncontrarOrden(id);
        }
    }
}
