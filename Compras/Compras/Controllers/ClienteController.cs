﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Compras.Models;
using Microsoft.EntityFrameworkCore;
using Compras.Services;
using Compras.dto;

namespace Compras.Controllers
{
    public class ClienteController : Controller
    {
       // private readonly ComprasContext _db;
        private readonly IClienteService _iClienteService;
        public ClienteController(
            IClienteService iClienteService
            //ComprasContext db
            )
        {
           // _db = db;
            _iClienteService = iClienteService;
        }
        public async Task<ActionResult> Index()
        {
            //var Clientes = _db.Clientes.ToList();
            List<dtoCliente> Clientes = await _iClienteService.ListarClientes();
            return View(Clientes);
        }

        public IActionResult Crear()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Crear([Bind("Nombre,Apellido")] dtoCliente cliente)
        {
            if (ModelState.IsValid)
            {
                var respuesta = await _iClienteService.CrearCliente(cliente);
                //_db.Add(cliente);
                //await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(cliente);

        }
        public async Task<IActionResult> Detalles(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //var persona = await _db.Clientes
            //    .FirstOrDefaultAsync(m => m.IdCliente == id);
            dtoCliente dtoCliente = await _iClienteService.ConsultarCliente(id);
            if (dtoCliente == null)
            {
                return NotFound();
            }

            return View(dtoCliente);
        }

        public async Task<IActionResult> Editar(int? id)
        {
            if (id == 0)
            {
                return RedirectToAction("Index");
            }
            dtoCliente cliente = await _iClienteService.ConsultarCliente(id);
            return View(cliente);
        }

        [HttpPost]
        public async Task<ActionResult> Editar(int id, [Bind("IdCliente,Nombre,Apellido")] dtoCliente cliente)
        {
            if (id != cliente.IdCliente)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var respuesta = await _iClienteService.ActualizarCliente(cliente);
                   
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await ClienteExists(cliente.IdCliente))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(cliente);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //var Cliente = await _db.Clientes
            //    .FirstOrDefaultAsync(m => m.IdCliente == id);
            dtoCliente cliente = await _iClienteService.ConsultarCliente(id);
            if (cliente == null)
            {
                return NotFound();
            }

            return View(cliente);
        }

       
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            //var persona = await _db.Clientes.FindAsync(id);
            //_db.Clientes.Remove(persona);
            //await _db.SaveChangesAsync();
            dtoCliente cliente = await _iClienteService.ConsultarCliente(id);
            bool result = await _iClienteService.EliminarCliente(cliente.IdCliente);
            return RedirectToAction(nameof(Index));
        }

        public async Task<bool> ClienteExists(int id)
        {
            bool result = await _iClienteService.EncontrarCliente(id);
            return result;
        }

    }
}
