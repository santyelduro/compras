﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Compras;
using Compras.Services;
using Compras.dto;

namespace Compras.Controllers
{
    public class ArticulosController : Controller
    {
        private readonly ComprasContext _context;
        private readonly IArticuloService _iArticuloService;
        public ArticulosController(
            IArticuloService iArticuloService,
            ComprasContext context)
        {
            _context = context;
            _iArticuloService = iArticuloService;
        }

        // GET: Articulos
        public async Task<ActionResult> Index()
        {
            List<dtoArticulo> articulos = await _iArticuloService.ListarArticulos();
            //View(await _context.Articulos.ToListAsync());
            return View(articulos);

        }

        // GET: Articulos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //var articulo = await _context.Articulos
            //    .FirstOrDefaultAsync(m => m.IdArticulo == id);
            dtoArticulo dtoArticulo = await _iArticuloService.ConsultarArticulo(id);
            if (dtoArticulo == null)
            {
                return NotFound();
            }

            return View(dtoArticulo);
        }

        // GET: Articulos/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Articulos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdArticulo,CodigoArticulo,NombreArticulo,PrecioUnitario,Stock")] dtoArticulo articulo)
        {
            if (ModelState.IsValid)
            {
                var respuesta = _iArticuloService.CreaArticulo(articulo);
                //_context.Add(articulo);
                //await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(articulo);
        }

        // GET: Articulos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == 0)
            {
                return RedirectToAction("Index");
            }

            dtoArticulo articulo = await _iArticuloService.ConsultarArticulo(id);//_context.Articulos.FindAsync(id);
            if (articulo == null)
            {
                return NotFound();
            }
            return View(articulo);
        }

        // POST: Articulos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdArticulo,CodigoArticulo,NombreArticulo,PrecioUnitario,Stock")] dtoArticulo articulo)
        {
            if (id != articulo.IdArticulo)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var respuesta = await _iArticuloService.ActualizarArticulo(articulo);
                    //_context.Update(articulo);
                    //await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await ArticuloExists(articulo.IdArticulo))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(articulo);
        }

        // GET: Articulos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //var articulo = await _context.Articulos
            //    .FirstOrDefaultAsync(m => m.IdArticulo == id);
            dtoArticulo articulo = await _iArticuloService.ConsultarArticulo(id);
            if (articulo == null)
            {
                return NotFound();
            }

            return View(articulo);
        }

        // POST: Articulos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            //var articulo = await _context.Articulos.FindAsync(id);
            //_context.Articulos.Remove(articulo);
            //await _context.SaveChangesAsync();
            dtoArticulo articulo = await _iArticuloService.ConsultarArticulo(id);
            bool result = await _iArticuloService.EliminarArticulo(articulo.IdArticulo);
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> ArticuloExists(int id)
        {
            //return _context.Articulos.Any(e => e.IdArticulo == id);
            bool result = await _iArticuloService.EncontrarArticulo(id);
            return result;
        }
    }
}
