﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Compras
{
    public partial class Articulo
    {
        public Articulo()
        {
            Ordenes = new HashSet<Ordene>();
        }

        public int IdArticulo { get; set; }
        public string CodigoArticulo { get; set; }
        public string NombreArticulo { get; set; }
        public decimal PrecioUnitario { get; set; }
        public int? Stock { get; set; }

        public virtual ICollection<Ordene> Ordenes { get; set; }
    }
}
