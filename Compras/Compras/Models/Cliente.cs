﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Compras
{
    public partial class Cliente
    {
        public Cliente()
        {
            Ordenes = new HashSet<Ordene>();
        }

        public int IdCliente { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }

        public virtual ICollection<Ordene> Ordenes { get; set; }
    }
}
