﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Compras
{
    public partial class Ordene
    {
        public int IdOrden { get; set; }
        public int IdCliente { get; set; }
        public int IdArticulo { get; set; }
        public DateTime? FechaOrden { get; set; }
        public int? Cantidad { get; set; }
        public int? StockSolicitado { get; set; }

        public virtual Articulo IdArticuloNavigation { get; set; }
        public virtual Cliente IdClienteNavigation { get; set; }
    }
}
