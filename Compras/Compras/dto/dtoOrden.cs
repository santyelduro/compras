﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compras.dto
{
    public class dtoOrden
    {
        public int IdOrden { get; set; }
        public int IdCliente { get; set; }

        public string NombreCliente { get; set; }
        public List<dtoCliente> Clientes { get; set; }
        public int IdArticulo { get; set; }
        public string NombreArticulo { get; set; }
        public List<dtoArticulo> Articulos { get; set; }
        public DateTime? FechaOrden { get; set; }
        public int? Cantidad { get; set; }
        public int? StockSolicitado { get; set; }
    }
}
