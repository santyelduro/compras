﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compras.dto
{
    public class dtoArticulo
    {
        public int IdArticulo { get; set; }
        public string CodigoArticulo { get; set; }
        public string NombreArticulo { get; set; }

        public int? Stock { get; set; }

        public decimal PrecioUnitario { get; set; }
    }
}
