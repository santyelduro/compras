﻿using Compras.dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compras.Services
{
    public interface IOrdenService
    {
        Task<List<dtoOrden>> ListarOrdenes();
        Task<bool> CrearOrden(dtoOrden dtoOrden);
        Task<dtoOrden> ConsultarOrden(int? id);
        Task<bool> ActualizarOrden(dtoOrden dtoOrden);
        Task<bool> EliminarOrden(int? id);
        Task<bool> EncontrarOrden(int? id);

    }
}
