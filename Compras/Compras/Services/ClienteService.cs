﻿using Compras.dto;
using Compras.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compras.Services
{
    public class ClienteService: IClienteService
    {
        private readonly IClienteRepository _iClienteRepository;

        public ClienteService(IClienteRepository iClienteRepository)
        {
            _iClienteRepository = iClienteRepository;
        }

        public async Task<List<dtoCliente>> ListarClientes()
        {
            List<dtoCliente> dtoClientes = new List<dtoCliente>();
            List<Cliente> clientes = await _iClienteRepository.ListarClientes();
            foreach (var cliente in clientes)
            {
                dtoCliente dtoCliente = new dtoCliente();
                dtoCliente.IdCliente = cliente.IdCliente;
                dtoCliente.Nombre = cliente.Nombre;
                dtoCliente.Apellido = cliente.Apellido;
                dtoClientes.Add(dtoCliente);
            }
            return dtoClientes;
        }

        public async Task<bool> CrearCliente(dtoCliente dtoCliente)
        {
            Cliente EntidadCliente = new Cliente();
            EntidadCliente.Nombre = dtoCliente.Nombre;
            EntidadCliente.Apellido = dtoCliente.Apellido;
            bool respuesta = await _iClienteRepository.CrearCliente(EntidadCliente);
            return respuesta;
        }

        public async Task<dtoCliente> ConsultarCliente(int? id)
        {
            Cliente EntidadCliente = new Cliente();
            dtoCliente dtoCliente = new dtoCliente();
            EntidadCliente = await _iClienteRepository.ConsultarCliente(id);
            dtoCliente.IdCliente = EntidadCliente.IdCliente;
            dtoCliente.Nombre = EntidadCliente.Nombre;
            dtoCliente.Apellido = EntidadCliente.Apellido;
            return dtoCliente;
        }

        public async Task<bool> ActualizarCliente(dtoCliente dtoCliente)
        {
            Cliente EntidadCliente = new Cliente();
            EntidadCliente.IdCliente = dtoCliente.IdCliente;
            EntidadCliente.Nombre = dtoCliente.Nombre;
            EntidadCliente.Apellido = dtoCliente.Apellido;
            bool respuesta = await _iClienteRepository.ActualizarCliente(EntidadCliente);
            return respuesta;
        }

        public async Task<bool> EliminarCliente(int? id)
        {
            bool respuesta = await _iClienteRepository.EliminarCliente(id);
            return respuesta;
        }

        public async Task<bool> EncontrarCliente(int? id)
        {
            bool respuesta = await _iClienteRepository.EncontrarCliente(id);
            return respuesta;
        }

    }
}
