﻿using Compras.dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compras.Services
{
    public interface IClienteService
    {
        Task<List<dtoCliente>> ListarClientes();
        Task<bool> CrearCliente(dtoCliente dtoCliente);
        Task<dtoCliente> ConsultarCliente(int? id);
        Task<bool> ActualizarCliente(dtoCliente dtoCliente);

        Task<bool> EliminarCliente(int? id);

        Task<bool> EncontrarCliente(int? id);
    }
}
