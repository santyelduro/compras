﻿using Compras.dto;
using Compras.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compras.Services
{
    public class OrdenService: IOrdenService
    {
        private readonly IOrdenRepository _iOrdenRepository;
        private readonly IArticuloRepository _iArticuloRepository;
        private readonly IClienteRepository _iClienteRepository;
        public OrdenService(
            IArticuloRepository iArticuloRepository,
            IOrdenRepository iOrdenRepository,
            IClienteRepository iClienteRepository
            )
        {
            _iOrdenRepository = iOrdenRepository;
            _iArticuloRepository = iArticuloRepository;
            _iClienteRepository = iClienteRepository;
        }

        public async Task<List<dtoOrden>> ListarOrdenes()
        {
            List<dtoOrden> dtoOrdenes = new List<dtoOrden>();
            List<Ordene> ordenes = await _iOrdenRepository.ListarOrdens();
            foreach (var orden in ordenes)
            {
                Cliente cliente = await _iClienteRepository.ConsultarCliente(orden.IdCliente);
                Articulo articulo = await _iArticuloRepository.ConsultarArticulo(orden.IdArticulo);

                dtoOrden dtoOrden = new dtoOrden();
                dtoOrden.IdOrden = orden.IdOrden;
                dtoOrden.NombreCliente = cliente.Nombre;
                dtoOrden.IdCliente = cliente.IdCliente;
                dtoOrden.NombreArticulo = articulo.NombreArticulo;
                dtoOrden.IdArticulo = articulo.IdArticulo;
                dtoOrden.FechaOrden = orden.FechaOrden;
                dtoOrden.Cantidad = orden.Cantidad;
              //  dtoOrden.StockSolicitado = orden.StockSolicitado;
                dtoOrdenes.Add(dtoOrden);
            }
            return dtoOrdenes;
        }

        public async Task<bool> CrearOrden(dtoOrden dtoOrden)
        {
            Articulo articulo = await _iArticuloRepository.ConsultarArticulo(dtoOrden.IdArticulo);

            int? StokDisponible = articulo.Stock;
            if (dtoOrden.Cantidad <= StokDisponible)
            {
                articulo.Stock = articulo.Stock - dtoOrden.Cantidad;
                await _iArticuloRepository.ActualizarArticulo(articulo);

                Ordene EntidadOrden = new Ordene();
                EntidadOrden.IdCliente = dtoOrden.IdCliente;
                EntidadOrden.IdArticulo = dtoOrden.IdArticulo;
                EntidadOrden.FechaOrden = dtoOrden.FechaOrden;
                EntidadOrden.Cantidad = dtoOrden.Cantidad;
               // EntidadOrden.StockSolicitado = dtoOrden.StockSolicitado;
                bool respuesta = await _iOrdenRepository.CrearOrden(EntidadOrden);
                return respuesta;
            }
            else
            {
                return false;
            }

           
           
        }

        public async Task<dtoOrden> ConsultarOrden(int? id)
        {
            Ordene EntidadOrden = new Ordene();
            dtoOrden dtoOrden = new dtoOrden();
            EntidadOrden = await _iOrdenRepository.ConsultarOrden(id);
            Cliente cliente = await _iClienteRepository.ConsultarCliente(EntidadOrden.IdCliente);
            Articulo articulo = await _iArticuloRepository.ConsultarArticulo(EntidadOrden.IdArticulo);
            dtoOrden.IdOrden = EntidadOrden.IdOrden;
            dtoOrden.NombreCliente = cliente.Nombre;
            dtoOrden.IdCliente = cliente.IdCliente;
            dtoOrden.NombreArticulo = articulo.NombreArticulo;
            dtoOrden.IdArticulo = articulo.IdArticulo;
            dtoOrden.FechaOrden = EntidadOrden.FechaOrden;
            dtoOrden.Cantidad = EntidadOrden.Cantidad;
           // dtoOrden.StockSolicitado = dtoOrden.StockSolicitado;
            return dtoOrden;
        }

        public async Task<bool> ActualizarOrden(dtoOrden dtoOrden)
        {
            Ordene EntidadOrden = new Ordene();
            EntidadOrden.IdCliente = dtoOrden.IdCliente;
            // EntidadOrden.IdClienteNavigation.IdCliente = dtoOrden.IdCliente;
            EntidadOrden.IdOrden = dtoOrden.IdOrden;
            EntidadOrden.IdArticulo = dtoOrden.IdArticulo;
           // EntidadOrden.IdArticuloNavigation.IdArticulo = dtoOrden.IdArticulo;
            EntidadOrden.FechaOrden = dtoOrden.FechaOrden;
            EntidadOrden.Cantidad = dtoOrden.Cantidad;
          //  EntidadOrden.StockSolicitado = dtoOrden.StockSolicitado;
            bool respuesta = await _iOrdenRepository.ActualizarOrden(EntidadOrden);
            return respuesta;
        }

        public async Task<bool> EliminarOrden(int? id)
        {
            bool respuesta = await _iOrdenRepository.EliminarOrden(id);
            return respuesta;
        }

        public async Task<bool> EncontrarOrden(int? id)
        {
            bool respuesta = await _iOrdenRepository.EncontrarOrden(id);
            return respuesta;
        }
    }
}
