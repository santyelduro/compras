﻿using Compras.dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compras.Services
{
    public interface IArticuloService
    {
        Task<List<dtoArticulo>> ListarArticulos();
        Task<bool> CreaArticulo(dtoArticulo dtoArticulo);
        Task<dtoArticulo> ConsultarArticulo(int? id);
        Task<bool> ActualizarArticulo(dtoArticulo dtoArticulo);
        Task<bool> EliminarArticulo(int? id);
        Task<bool> EncontrarArticulo(int? id);

    }
}
