﻿using Compras.dto;
using Compras.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compras.Services
{
    public class ArticuloService: IArticuloService
    {
        private readonly IArticuloRepository _iArticuloRepository;
        public ArticuloService(IArticuloRepository iArticuloRepository)
        {
            _iArticuloRepository = iArticuloRepository;
        }

        public async Task<List<dtoArticulo>> ListarArticulos()
        {
            List<dtoArticulo> dtoArticulos = new List<dtoArticulo>();
            List<Articulo> articulos = await _iArticuloRepository.ListarArticulos();
            foreach (var articulo in articulos)
            {
                dtoArticulo dtoArticulo = new dtoArticulo();
                dtoArticulo.IdArticulo = articulo.IdArticulo;
                dtoArticulo.CodigoArticulo = articulo.CodigoArticulo;
                dtoArticulo.NombreArticulo = articulo.NombreArticulo;
                dtoArticulo.PrecioUnitario = articulo.PrecioUnitario;
                dtoArticulo.Stock = articulo.Stock;
                dtoArticulos.Add(dtoArticulo);
            }
            return dtoArticulos;
        }

        public async Task<bool> CreaArticulo(dtoArticulo dtoArticulo)
        {
            Articulo EntidadArticulo = new Articulo();
            EntidadArticulo.CodigoArticulo = dtoArticulo.CodigoArticulo;
            EntidadArticulo.NombreArticulo = dtoArticulo.NombreArticulo;
            EntidadArticulo.PrecioUnitario = dtoArticulo.PrecioUnitario;
            EntidadArticulo.Stock = dtoArticulo.Stock;
            bool respuesta = await _iArticuloRepository.CrearArticulo(EntidadArticulo);
            return respuesta;
        }
        public async Task<dtoArticulo> ConsultarArticulo(int? id)
        {
            Articulo EntidadArticulo = new Articulo();
            dtoArticulo dtoArticulo = new dtoArticulo();

            EntidadArticulo = await _iArticuloRepository.ConsultarArticulo(id);
            dtoArticulo.IdArticulo = EntidadArticulo.IdArticulo;
            dtoArticulo.CodigoArticulo = EntidadArticulo.CodigoArticulo;
            dtoArticulo.NombreArticulo = EntidadArticulo.NombreArticulo;
            dtoArticulo.PrecioUnitario = EntidadArticulo.PrecioUnitario;
            dtoArticulo.Stock = EntidadArticulo.Stock;
            return dtoArticulo;
        }

        public async Task<bool> ActualizarArticulo(dtoArticulo dtoArticulo)
        {
            Articulo EntidadArticulo = new Articulo();
            EntidadArticulo.IdArticulo = dtoArticulo.IdArticulo;
            EntidadArticulo.CodigoArticulo = dtoArticulo.CodigoArticulo;
            EntidadArticulo.NombreArticulo = dtoArticulo.NombreArticulo;
            EntidadArticulo.PrecioUnitario = dtoArticulo.PrecioUnitario;
            EntidadArticulo.Stock = dtoArticulo.Stock;
            bool respuesta = await _iArticuloRepository.ActualizarArticulo(EntidadArticulo);
            return respuesta;
        }

        public async Task<bool> EliminarArticulo(int? id)
        {
            bool respuesta = await _iArticuloRepository.EliminarArticulo(id);
            return respuesta;
        }

        public async Task<bool> EncontrarArticulo(int? id)
        {
            bool respuesta = await _iArticuloRepository.EncontrarArticulo(id);
            return respuesta;
        }

    }
}
