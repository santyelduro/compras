﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Compras.Migrations
{
    public partial class Cambios : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ARTICULO",
                columns: table => new
                {
                    ID_ARTICULO = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CODIGO_ARTICULO = table.Column<string>(type: "varchar(10)", unicode: false, maxLength: 10, nullable: true),
                    NOMBRE_ARTICULO = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    PRECIO_UNITARIO = table.Column<decimal>(type: "decimal(18,2)", unicode: false, maxLength: 50, nullable: false),
                    Stock = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ARTICULO", x => x.ID_ARTICULO)
                        .Annotation("SqlServer:Clustered", false);
                });

            migrationBuilder.CreateTable(
                name: "CLIENTE",
                columns: table => new
                {
                    ID_CLIENTE = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NOMBRE = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    APELLIDO = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CLIENTE", x => x.ID_CLIENTE)
                        .Annotation("SqlServer:Clustered", false);
                });

            migrationBuilder.CreateTable(
                name: "ORDENES",
                columns: table => new
                {
                    ID_CLIENTE = table.Column<int>(type: "int", nullable: false),
                    ID_ARTICULO = table.Column<int>(type: "int", nullable: false),
                    IdOrden = table.Column<int>(type: "int", nullable: false),
                    FECHA_ORDEN = table.Column<DateTime>(type: "datetime", nullable: true),
                    CANTIDAD = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ORDENES", x => new { x.ID_CLIENTE, x.ID_ARTICULO })
                        .Annotation("SqlServer:Clustered", false);
                    table.ForeignKey(
                        name: "FK_ORDENES_RELATIONS_ARTICULO",
                        column: x => x.ID_ARTICULO,
                        principalTable: "ARTICULO",
                        principalColumn: "ID_ARTICULO",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ORDENES_RELATIONS_CLIENTE",
                        column: x => x.ID_CLIENTE,
                        principalTable: "CLIENTE",
                        principalColumn: "ID_CLIENTE",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "RELATIONSHIP_2_FK",
                table: "ORDENES",
                column: "ID_ARTICULO");

            migrationBuilder.CreateIndex(
                name: "RELATIONSHIP_3_FK",
                table: "ORDENES",
                column: "ID_CLIENTE");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ORDENES");

            migrationBuilder.DropTable(
                name: "ARTICULO");

            migrationBuilder.DropTable(
                name: "CLIENTE");
        }
    }
}
